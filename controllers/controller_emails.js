const express = require("express");
const app = express();
const Mail = require("../models/mail");
const mongoose = require("mongoose");
const dbAccess = require("../config/dbAccess");

/**
 * @swagger
 * /api/email/{email}:
 *   get:
 *     tags:
 *       - email
 *     parameters:
 *       - in: path
 *         name: email
 *         type: string
 *         required: true
 *         format: string
 *     description: Busca en Mongo el email
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: devuelve json con la busqueda
 *       400:
 *         description: devuelve json avisando del error
 *
 */
app.get("/:email", function(req, res) {
  let emailparam = req.params.email;

  mongoose.connect(
    dbAccess.prototype.connectURLToMailsDB,
    { useNewUrlParser: true },
    (err, res) => {
      if (err) throw err;
    }
  );

  Mail.find({ email: emailparam }, function(err, mails) {
    if (err) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      mails
    });
  });
});

/**
 * @swagger
 * /api/email/:
 *   post:
 *     tags:
 *       - email
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           properties:
 *             email:
 *               type: string
 *             contenido:
 *               type: string 
 *         required:
 *           - email
 *           - contenido
 *     responses:
 *       200:
 *         description: Email insertado en mongo con exito
 *       401:
 *         description: Token invalido, no tiene permisos para ejecutar esta api
 *       400:
 *         description: Ocurrio un error al guardar el mail en mongo
 */
app.post("/", function(req, res) {
  //inserta usuario en mongo que llega en BODY
  let body = req.body;

  let mail = new Mail({
    email: body.email,
    fecha: new Date(),
    contenido: body.contenido
  });

  mongoose.connect(
    dbAccess.prototype.connectURLToMailsDB,
    { useNewUrlParser: true },
    (err, res) => {
      if (err) {
        return res.status(400).json({
          ok: false,
          err
        });
      }
    }
  );

  mail.save((err, mailDB) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      mail: mailDB
    });
  });
});

/**
 * @swagger
 * /api/email/getMailsByContent/{textoContenido}:
 *   get:
 *     tags:
 *       - email
 *     parameters:
 *       - in: path
 *         name: contenido
 *         type: string
 *         required: true
 *         format: string
 *     description: Busca en Mongo el email por contenido
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: devuelve json con la busqueda
 *       400:
 *         description: devuelve json avisando del error
 *
 */
app.get("/getMailsByContent/:textoContenido", function(req, res) {
  let paramContenido = req.params.textoContenido;

  mongoose.connect(
    dbAccess.prototype.connectURLToMailsDB,
    { useNewUrlParser: true },
    (err, res) => {
      if (err) throw err;
    }
  );

  Mail.find({ contenido: { $regex: new RegExp(paramContenido) } }, function(
    err,
    mails
  ) {
    if (err) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      mails
    });
  });
});

module.exports = app;
