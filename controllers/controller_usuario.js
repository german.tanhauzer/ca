const express = require("express");
const app = express();
const Usuario = require("../models/usuario");
const mongoose = require("mongoose");
const dbAccess = require("../config/dbAccess");
const { verificaTokenMiddleware } = require("../middlewares/verificaTokenMiddleware");

app.get("/:email", function(req, res) {
  let emailparam = req.params.email;

  mongoose.connect(
    dbAccess.prototype.connectURLToUsersDB,
    { useNewUrlParser: true },
    (err, res) => {
      if (err) throw err;
    }
  );

  Usuario.findOne({ email: emailparam }, function(err, usuario) {
    if (err) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      usuario
    });
  });
});

app.post("/", verificaTokenMiddleware,function(req, res) {
  //inserta usuario en mongo que llega en BODY
  let body = req.body;

  let usuario = new Usuario({
    nombre: body.nombre,
    email: body.email,
    contactInfo: {
      tel: body.contactInfo.tel,
      address: {
        city: body.contactInfo.address.city,
        street: body.contactInfo.address.street,
        houseNumber: body.contactInfo.address.houseNumber
      }
    }
  });

  mongoose.connect(
    dbAccess.prototype.connectURLToUsersDB,
    { useNewUrlParser: true },
    (err, res) => {
      if (err) throw err;
    }
  );

  usuario.save((err, usuarioDB) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      usuario: usuarioDB
    });
  });
});

app.get("/getUsersByCity/:city", function(req, res) {
  //devuelvo todos los usuarios
  let paramCity = req.params.city;

  mongoose.connect(
    dbAccess.prototype.connectURLToUsersDB,
    { useNewUrlParser: true },
    (err, res) => {
      if (err) throw err;
    }
  );

  Usuario.find({ "contactInfo.address.city": paramCity }, function(
    err,
    usuarios
  ) {
    console.log(usuarios);
    if (err) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      usuarios
    });
  });
});

module.exports = app;
