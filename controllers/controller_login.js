const express = require("express");
const app = express();
var jwt = require("jsonwebtoken");

app.post("/", async function(req, res) {
  let usuario, password;
  usuario = req.body.usuario;
  password = req.body.password;

  if (usuario === "admin" && password === "123456") {
    var tokenData = { username: usuario,ultimoLogin:new Date() };

    var token = jwt.sign(tokenData, process.env.CLAVEJWT, {
      expiresIn: 60 * 60 * 24 // expira en 24 horas
    });

    res.send({
      token
    });

  } else {
    res.status(401).send({
      error: "usuario o contraseña inválidos"
    });
    return;
  }
});

module.exports = app;
