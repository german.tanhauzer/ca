let chai = require("chai");
let chaihttp = require("chai-http");
//incluir el server.js para que mocha pueda ejecutar las pruebas
let server = require("../server");
let should = chai.should();
let token="";

chai.use(chaihttp);

describe("Prueba Login: ", function () {
    it("Probamos un login tal como sera en produccion", (done) => {
        chai.request(server)
            .post("/api/login/")
            .send({
                usuario: "admin",
                password: "123456"
            })
            .end((err, res) => {
                res.body.should.be.a('Object');

                chai.assert.equal(res.status, 200, "No devuelve status code 200");
                chai.assert.notEqual(res.body.token, null, "No devolvio Token");
                res.body.should.be.a("Object");
                token = res.body.token;        
                //console.log("devolvio token: " + token);
                done();
            });
    });
    it("Probamos un login tal como sera en produccion fallando", (done) => {
        chai.request(server)
            .post("/api/login/")
            .send({
                usuario: "admin",
                password: "12345"
            })
            .end((err, res) => {
                res.body.should.be.a('Object');

                chai.assert.equal(res.status, 200, "No devuelve status code 200");
                chai.assert.notEqual(res.body.token, null, "No devolvio Token");
                res.body.should.be.a("Object");
                token = res.body.token;        
                //console.log("devolvio token: " + token);
                done();
            });
    });
});