require("./config/config");

const express = require('express');
const app = express();

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());

/*
**********  CONFIGURACION DE SWAGGER  **************
*/

//paquetes npm para que swagger funcione
const swaggerUi = require('swagger-ui-express')
const swaggerJsdoc = require('swagger-jsdoc');

//la especificacion que figurara en la doc automatica
const swaggerDefinition = {
  info: {
    title: "REST API ORT - CA",
    version: "1.0.0",
    description: "Endpoints to test the user registration routes"
  },
  host: "localhost:" + process.env.PORT,
  basePath: "/"
};

//donde va a "mirar" swagger para exponer la doc
const options = {
  swaggerDefinition,
  apis: ["./controllers/*.js"],
  customCss: '.swagger-ui .topbar { display: none }'
};

//finalizacion de las configuraciones
const swaggerSpec = swaggerJsdoc(options);
app.get('/swagger.json', function(req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(swaggerSpec);
});

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
/*
**********  FIN CONFIGURACION DE SWAGGER  **************
*/
app.use("/api/login/", require("./controllers/controller_login"));
app.use("/api/usuario/", require("./controllers/controller_usuario"));
app.use("/api/email/", require("./controllers/controller_emails"));
app.use("/html", express.static("static_files"));

//ahora toma el puerto del archivo config/config.js
app.listen(process.env.PORT, function() {
  console.log(
    "Servidor express iniciado en el puerto " + process.env.PORT + "!"
  );
});

//DEBO EXPORTAR app PARA LOS TEST EN MOCHA, solo para eso, por ahora queda comentado
module.exports = app;