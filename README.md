# Instalaciones Necesarias:

Comenzaremos instalando el entorno de desarrollo

*  Visual studio CODE: https://code.visualstudio.com/download 
*  NPM : https://www.npmjs.com/get-npm 
*  NODEJS: https://nodejs.org/es/download/
*  POSTMAN: https://www.softwaretestingmaterial.com/install-postman/
*  CHROME

Una vez tengan todo instalado, iremos avanzando en el curso utilizando NPM cuando sea necesario agregar dependencias por ej. Express

# CI  / CD
#Gitlab ofrece la posibilidad de realizar integracion continua y desarrollo continuo , pudiendo correr test, build y deploys automaticos 
A modo de ejemplo dejo implementado en Heroku el sistema una vez se hace commit en git al branch master, automaticamente se realiza el deploy a https://ort-nodejs.herokuapp.com/

npm init
inicia todo el proyecto (crea package.json)

npm install express (agrega la dependencia de express al proyecto)

npm start (inicia el programa) o node index.js