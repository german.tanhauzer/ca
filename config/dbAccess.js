class dbAccess {
  get connectURLToUsersDB() {
    return process.env.URLDB + "usuarios";
  }
  get connectURLToMailsDB() {
    return process.env.URLDB + "mails";
  }
}

module.exports = dbAccess;