const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let usuarioSchema = new Schema({
    email: {
        type: String,
        unique: true,
        required: [true, 'El correo es necesario']
    },
    nombre: {
        type: String,
        required: [true, 'El nombre es necesario']
    },
    contactInfo: {
        tel: [Number],
        address: {
            city: String,
            street: String,
            houseNumber: String
        }
    }
});

/*
usuarioSchema.methods.toJSON = function() {

    let user = this;
    let userObject = user.toObject();
    //no devuelvo el password por seguridad
    delete userObject.password;

    return userObject;
}
*/

usuarioSchema.plugin(uniqueValidator, { message: '{PATH} debe de ser único' });


module.exports = mongoose.model('Usuario', usuarioSchema);