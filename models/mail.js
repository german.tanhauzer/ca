const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let mailSchema = new Schema({
    email: {
        type: String,
        required: [true, 'El correo es necesario']
    },
    fecha: {
        type: Date,
        required: [true, 'La fecha es necesaria']
    },
    contenido:{
        type:String,
        required:[true,'El mail debe tener contenido']
    }
});

mailSchema.index({email: 'text', contenido: 'text'});
mailSchema.plugin(uniqueValidator, { message: '{PATH} debe de ser único' });


module.exports = mongoose.model('Mail', mailSchema);