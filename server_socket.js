const WebSocket = require("ws");

const wss = new WebSocket.Server({ port: 8080 });
let clientes = new Array();

wss.on("connection", ws => {
  let id = clientes.length;
  clientes.push(ws);

  ws.on("message", message => {
    mensajeParaTodos(message);
    console.log(`mensaje recibido => ${message}`);
  });

  ws.on("close", function(reasonCode, description) {
    clientes.splice(id, 1);
    mensajeParaTodos("Se fue un cliente del chat");
    mensajeParaTodos("Clientes aun conectados " + clientes.length);
  });

  ws.send("Bievenido al server de CA");
  mensajeParaTodos("Clientes conectados " + clientes.length);
});

function mensajeParaTodos(mensaje) {
  clientes.map(cliente => cliente.send(mensaje));
}
